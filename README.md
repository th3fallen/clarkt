# README #

Very simple oAuth application using LinkedIn's api to automatically maintain and populate my resume.

### Stack
- PHP 5.6
- [OAuth](http://pecl.php.net/package/oauth) - Linkedin API
- Redis
- [Rocketeer](http://rocketeer.autopergamene.eu/) - Deployment

- Composer
- -  vlucas/phpdotenv
- - segmentio/analytics-php 
- - predis/redis

- Bower
- - jQuery
- - jQuery-PrettyPhoto
- - Bourbon
- - Neat
- - Font-Awesome

### Contributions/forks welcome


### Todo:
 - Move to Guzzle instead of pecl OAuth

### Dependency Status(s)
[![Dependency Status](https://www.versioneye.com/user/projects/555e1d1b634daacd41000df0/badge.svg?style=flat)](https://www.versioneye.com/user/projects/555e1d1b634daacd41000df0)

[![Dependency Status](https://www.versioneye.com/user/projects/555e1d1c634daa5dc8000f44/badge.svg?style=flat)](https://www.versioneye.com/user/projects/555e1d1c634daa5dc8000f44)