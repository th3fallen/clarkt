<?php
namespace ClarkT\OAuth;

use ClarkT\Cache;
use OAuth;

/**
 * Class ApiService
 *
 * @package ClarkT\OAuth
 */
class ApiService
{

    /**
     * @var OAuth;
     */
    protected $client;

    /**
     *
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * Connect to api using keys and access tokens
     *
     * @return OAuth
     */
    public function connect()
    {
        /*
         * Set the consumer key and secret to authenticate the api
         */
        $consumerKey = $_ENV['CONSUMER_KEY'];
        $consumerSecret = $_ENV['CONSUMER_SECRET'];

        /*
        * Manually inject access key and secret so the site will only use my information
        */
        $accessToken = $_ENV['ACCESS_TOKEN'];
        $accessSecret = $_ENV['ACCESS_SECRET'];

        $this->client = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1,
            OAUTH_AUTH_TYPE_AUTHORIZATION);
        $this->client->setNonce(rand());

        $this->client->setToken($accessToken, $accessSecret);

        return $this->client;
    }

    /**
     * Return our profile information as json object and cache it
     *
     * @return string
     */
    public function getProfile()
    {
        $cache = new Cache();

        $profile = $cache->get('profile');


        if ($profile) {
            return $profile;
        }
        $this->client->fetch(
            'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,phone-numbers,main-address,summary,industry,headline,positions,skills,languages,educations,public-profile-url)?format=json'
        );
        $response_info = $this->client->getLastResponse();
        $cache->set('profile', $response_info);

        return $response_info;
    }

}
