<?php
/**
 * @author Clark Tomlinson  <fallen013@gmail.com>
 * @since 2/13/13, 10:34 AM
 * @link http://www.clarkt.com
 * @copyright Clark Tomlinson © 2013
 *
 */
namespace ClarkT;

use ClarkT\OAuth\ApiService;
use OAuth\Common\Http\Uri\UriFactory;

class Core
{
    public static function start()
    {
        \Dotenv::load(dirname(__DIR__));

        $oauth = new ApiService();

        $user = json_decode($oauth->getProfile());

        return $user;
    }

    public static function bootstrap()
    {
        /**
         * Setup the timezone
         */
        ini_set('date.timezone', 'America/New_York');

        /**
         * Create a new instance of the URI class with the current URI, stripping the query string
         */
        $uriFactory = new UriFactory();
        $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
        $currentUri->setQuery('');

        return $currentUri;
    }
}
