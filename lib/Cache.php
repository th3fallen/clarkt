<?php
/**
 * @author Clark Tomlinson  <${personalEmail}>
 * @since 4/28/14, 9:25 AM
 * @link ${personalWebsite}
 * @copyright Clark Tomlinson © 2014
 *
 */

namespace ClarkT;


use Predis\Client;

/**
 * Class Cache
 *
 * @package ClarkT
 */
class Cache
{

    /**
     * @var \Predis\Client
     */
    private $client;

    /**
     * @return \ClarkT\Cache
     */
    public function __construct()
    {
        $this->client = new Client(
            [
                'scheme' => 'unix',
                'path'   => '/var/run/redis/redis.sock'
            ]
        );
        return $this->client;
    }

    public function get($key)
    {
        return $this->client->get($key);
    }

    public function set($key, $value, $ttl = 2592000)
    {
        $set = $this->client->set($key, $value);
        $this->client->expire($key, $ttl);

        return $set;

    }
}
