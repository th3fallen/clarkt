<!DOCTYPE html>
<!--
Hello source code viewer!

This resume is a work in progress.
I'm no designer, this template was proudly purchased from here:
http://themeforest.net/item/awesome-online-resumecv/81850
Hopefully one day I can tweak it enough to make it personal.

Thanks,
Clark Tomlinson
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php
    use ClarkT\Core;

    require_once('vendor/autoload.php');

    $user = Core::start();
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $user->firstName . ' ' . $user->lastName; ?> - R&eacute;sum&eacute;</title>


    <link type="text/css" rel="stylesheet" href="assets/css/style.css"/>
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css"/>
    <link type="text/css" rel="stylesheet" href="assets/css/print.css" media="print"/>
    <link rel="Shortcut Icon" href="favicon.ico" type="image/x-icon"/>

    <!--[if lt IE 8]>
    <script src="//ie7-js.googlecode.com/svn/version/2.0(beta3)/IE8.js" type="text/javascript"></script>
    <![endif]-->
    <!-- Load Analytics -->
</head>
<body>
<div id="container">
    <div id="sidebar" equalize>
        <h1 class="profileheader"><?php echo $user->firstName . ' ' . $user->lastName; ?></h1>

        <div id="frame">
            <a href="assets/images/headshot.jpg" rel="prettyPhoto">
                <img src="assets/images/headshot_small.jpg" alt="Clark Tomlinson"/>
            </a>
        </div>
        <ul id="details">
            <li><span class="heading">Twitter</span>
                <a id="twitter" href="http://twitter.com/th3fallen" target="_blank">@Th3fallen</a>
            </li>
            <li>
                <span class="heading">Email</span>
                <a href="mailto:<?php echo $user->emailAddress; ?>">
                    <?php echo $user->emailAddress; ?>
                </a>
            </li>
            <li><span class="heading">Location</span>
                Charlotte, NC
            </li>
        </ul>
        <!-- end details -->
    </div>
    <!-- end sidebar -->
    <div id="content" equalize>

        <div id="top">
            <ul class="inline-list">
                <li id="download" data-popup="true" title="Save My Resume">
                    <a target="_blank"
                       href="http://www.linkedin.com/profile/pdf?id=85403315&locale=en_US&pdfFileName=ClarkTomlinson&disablePdfCompression=true&trk=pdf_pro_full">
                        <i class="fa fa-2x fa-save"></i>
                    </a>
                </li>
                <li id="print" data-popup="true" title="Print My Resume">
                    <a href="javascript:window.print()">
                        <i class="fa fa-2x fa-print"></i>
                    </a>
                </li>
                <li id="mail" data-popup="true" title="Email Me">
                    <a href="mailto:<?php echo $user->emailAddress; ?>">
                        <i class="fa fa-2x fa-envelope-square"></i>
                    </a>
                </li>
                <li id="linkedin" data-popup="true" title="Visit my LinkedIn">
                    <a href="<?php echo $user->publicProfileUrl; ?>" rel="nofollow">
                        <i class="fa fa-2x fa-linkedin-square"></i>
                    </a>
                </li>
                <li id="github" data-popup="true" title="Visit my Github">
                    <a href="https://www.github.com/th3fallen" rel="nofollow">
                        <i class="fa fa-2x fa-github-square"></i>
                    </a>

                </li>
                <li id="bitbucket" data-popup="true" title="Visit my Bitbucket">
                    <a href="https://www.bitbucket.org/th3fallen" rel="nofollow">
                        <i class="fa fa-2x fa-bitbucket-square"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end of top -->

        <!-- begin the resume content -->

        <!-- begin section -->
        <h1 class="ribbon green">Current employment</h1>

        <?php
        foreach ($user->positions->values as $position) :
            if ($position->isCurrent) :
                ?>
                <div class="job">
                    <h2><?php echo $position->company->name; ?>
                        <small><?php echo DateTime::createFromFormat('!m',
                                    $position->startDate->month)->format('M') . ' ' . $position->startDate->year . ' - Current'; ?></small>
                    </h2>
                    <div class="jobTitle"><?php echo $position->title; ?></div>
                    <div class="jobDescription"><?php echo nl2br($position->summary); ?></div>
                    <!--                <hr/>-->
                </div>
                <!-- horizontal rule -->
                <!-- begin section -->
                <h1 class="ribbon red">Previous employment</h1>
            <?php endif; ?>


            <?php
            if (!$position->isCurrent) :
                ?>

                <div class="job">
                    <h2><?php echo $position->company->name; ?>
                        <small><?php echo DateTime::createFromFormat('!m',
                                    $position->startDate->month)->format('M') . ' ' . $position->startDate->year . ' - ' . DateTime::createFromFormat('!m',
                                    $position->endDate->month)->format('M') . ' ' . $position->endDate->year; ?></small>
                    </h2>
                    <div class="jobTitle"><?php echo $position->title; ?></div>
                    <div class="jobDescription"><?php echo nl2br($position->summary); ?></div>
                    <hr/>
                </div>
                <!-- horizontal rule -->
            <?php endif; endforeach; ?>
        <!-- begin section -->

        <h1 class="ribbon purple">Professional skills</h1>

        <p><?php echo $user->summary; ?></p>

        <div class="skills">
            <?php
            $chunks = array_chunk($user->skills->values, 10, true);

            foreach ($chunks as $chunk) {
                $set = '<ul>';
                foreach ($chunk as $skill) {
                    $set .= '<li>' . $skill->skill->name . '</li>';
                }

                $set .= "</ul>";
                echo $set;
            }

            ?>
        </div>
        <hr/>
        <!-- end section -->

        <!-- begin section -->
        <h1 class="ribbon orange">Education</h1>
        <?php
        foreach ($user->educations->values as $education) :
            ?>
            <h2><?php echo $education->schoolName; ?>
                <small><?php echo $education->startDate->year . ' - ' . $education->endDate->year ?></small>
            </h2>
            <div class="jobTitle"><?php echo $education->fieldOfStudy; ?></div>
            <!-- horizontal rule -->

        <?php endforeach; ?>
        <!-- end section -->

    </div>
    <!-- end content -->
</div>
<script type="text/javascript" src="assets/vendor/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/min/app.min.js"></script>

<!-- end container -->
</body>
</html>
