

//@prepros-prepend '../vendor/jquery-prettyPhoto/js/jquery.prettyPhoto.js';
var App = {
    init: function () {
        this.addDomEvents();
        //this.checkWindowSize();
    },
    addDomEvents: function () {
        this.startPopups();
        this.startPrettyPhoto();
        this.addAnalytics();
        this.equalizeHeights();
    },

    removeHashLinks: function () {

    },


    equalizeHeights: function () {
        $('[equalize]').equalHeight();
    },

    startPopups: function () {
        // todo: solve popup issue on linkedin button
        //$('li[data-popup]').popup({
        //    on: 'hover',
        //    variation: 'inverted'
        //});
    },

    initAnalytics: function () {
        window.analytics = window.analytics || [], window.analytics.methods = ["identify", "group", "track", "page", "pageview", "alias", "ready", "on", "once", "off", "trackLink", "trackForm", "trackClick", "trackSubmit"], window.analytics.factory = function (t) {
            return function () {
                var a = Array.prototype.slice.call(arguments);
                return a.unshift(t), window.analytics.push(a), window.analytics;
            };
        };
        for (var i = 0; i < window.analytics.methods.length; i++) {
            var key = window.analytics.methods[i];
            window.analytics[key] = window.analytics.factory(key);
        }
        window.analytics.load = function (t) {
            if (!document.getElementById("analytics-js")) {
                var a = document.createElement("script");
                a.type = "text/javascript", a.id = "analytics-js", a.async = !0, a.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.io/analytics.js/v1/" + t + "/analytics.min.js";
                var n = document.getElementsByTagName("script")[0];
                n.parentNode.insertBefore(a, n);
            }
        }, window.analytics.SNIPPET_VERSION = "2.0.9",
            window.analytics.load("34zk67rwov");
        window.analytics.page();
    },

    addAnalytics: function () {
        // Add Link Tracking for relevant links
        var links = {
            'Downloaded CV': $('#download a'),
            'Printed CV': $('#print a'),
            'Emailed Me': $('#mail a'),
            'Visited LinkedIn Profile': $('#linkedin a'),
            'Visited Twitter Profile': $('#twitter')
        };

        $.each(links, function (key, value) {
            analytics.trackLink(value, key);
        });
    },

    startPrettyPhoto: function () {
        $("a[rel^='prettyPhoto']").prettyPhoto({
            theme: 'light_rounded' /* light_rounded / dark_rounded / light_square / dark_square / facebook */
        });
    }

    //checkWindowSize: function () {
    //    var $sidebar = jQuery('#sidebar');
    //
    //    if (jQuery(window).height() < 620) {
    //        $sidebar.removeClass('fixed');
    //    } else {
    //        $sidebar.addClass('fixed');
    //    }
    //}
};


//jQuery(window).resize(App.checkWindowSize());

// Start analytics before we init the app js
App.initAnalytics();

jQuery(document).ready(function () {


    /**
     * Set all elements within the collection to have the same height.
     */
    $.fn.equalHeight = function(){
        var heights = [];
        $.each(this, function(i, element){
            $element = $(element);
            var element_height;
            // Should we include the elements padding in it's height?
            var includePadding = ($element.css('box-sizing') == 'border-box') || ($element.css('-moz-box-sizing') == 'border-box');
            if (includePadding) {
                element_height = $element.innerHeight();
            } else {
                element_height = $element.height();
            }
            heights.push(element_height);
        });
        this.css('height', Math.max.apply(window, heights) + 'px');
        return this;
    };

    /**
     * Create a grid of equal height elements.
     */
    $.fn.equalHeightGrid = function(columns){
        var $tiles = this;
        $tiles.css('height', 'auto');
        for (var i = 0; i < $tiles.length; i++) {
            if (i % columns === 0) {
                var row = $($tiles[i]);
                for(var n = 1;n < columns;n++){
                    row = row.add($tiles[i + n]);
                }
                row.equalHeight();
            }
        }
        return this;
    };

    /**
     * Detect how many columns there are in a given layout.
     */
    $.fn.detectGridColumns = function() {
        var offset = 0, cols = 0;
        this.each(function(i, elem) {
            var elem_offset = $(elem).offset().top;
            if (offset === 0 || elem_offset == offset) {
                cols++;
                offset = elem_offset;
            } else {
                return false;
            }
        });
        return cols;
    };

    /**
     * Ensure equal heights now, on ready, load and resize.
     */
    $.fn.responsiveEqualHeightGrid = function() {
        var _this = this;
        function syncHeights() {
            var cols = _this.detectGridColumns();
            _this.equalHeightGrid(cols);
        }
        $(window).bind('resize load', syncHeights);
        syncHeights();
        return this;
    };


    App.init();

});
